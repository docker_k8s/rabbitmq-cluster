Na hitro sem se malo poigral z vrstami v RabbitMQ, zanimal me ni performačni vidik, ampak možna izguba sami podatkov.
Mislim, da imamo RabbitMQ lahko hitro osvojen, ker ni nek kunšt. V slabih 2urah sem imel cluster postavljen in do izgube podatkov lahko pride samo v primeru, da padejo vsi nodi in se napačen prvi pobere. Če ne padejo vsi naenkrat imamo zelo malo možnosti za izgubo podatkov.

Postavitev: docker-3-node-rabbitmq-cluster

Kaj sem testiral, ugašanje nodov, prižiganje, ohranjanje sporočil. Ugotovitve:
1. Vrsta mora biti označena kot DURABLE (prav tako exchange), če želimo ohraniti vrsto po padcu oz. restartu
boolean durable = true;
channel.queueDeclare(QUEUE_NAME, durable, false, false, null);
2. Povežemo se z adapterjem na vse naslove:
Address[] address = {new Address("localhost", 5672), new Address("localhost", 5673), new Address("localhost", 5674)};
Naš client se poveže na enega izmed addressov. Nato moramo skonfigurirati MIRROR policy na rabbitMq (glej spodaj).

V primeru, da bi imeli 1 node cluster, če slučajno pade in da ne izgubiš sporočil, lahko daš na message persisent. In ko se pobere nazaj imamo vsa sporočila še vedno v vrsti.
channel.basicPublish("", QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes("UTF-8"));

V primeru da imamo 3-node cluster, imamo MIRRORing enabled in pošiljamo sporočila v cluster (klient se poveže na enega izmed nodov). V primeru, da vsi nodi padejo dol naenkrat, lahko pride do izgube sporočil, če se ne pobere prvi tisti na katerega smo pisali. (redek case, da padejo vsi 3je naenkrat).

Kako narediti MIRROR (repliciranje vrst), dodati je potrebno policy:

Replicaramo vse vrste, ki se začnejo n ha.
rabbitmqctl set_policy ha-all "^ha\." '{"ha-mode":"all"}'


V primeru če želimo, da se med seboj avtomatično sihronizirajo, tudi če se kakšna kasneje pridruži:
rabbitmqctl set_policy ha-two "^two\." \
   '{"ha-mode":"exactly","ha-params":2,"ha-sync-mode":"automatic"}'


V primeru če želimo replikacijo omejiti na določene node (smiselno pri večjih clustrih, ker je nepotrebno 10x replicirati podatek).
rabbitmqctl set_policy ha-nodes "^nodes\." \
   '{"ha-mode":"nodes","ha-params":["rabbit@nodeA", "rabbit@nodeB"]}'

Več: https://www.rabbitmq.com/ha.html
RabbitMQ - Highly Available (Mirrored) Queues

