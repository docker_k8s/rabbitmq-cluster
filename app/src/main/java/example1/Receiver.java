package example1;

import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * Created by zaletel on 31.3.2017.
 */
public class Receiver {

    private final static String QUEUE_NAME = "ha.hello";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername("user");
        factory.setPassword("password");
        Address[] address = {new Address("localhost", 5672), new Address("localhost", 5673), new Address("localhost", 5674)};
        Connection connection = factory.newConnection(address);
        Channel channel = connection.createChannel();

        boolean durable = true;
        channel.queueDeclare(QUEUE_NAME, durable, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println(" [x] Received '" + message + "'");
            }
        };
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }

}
