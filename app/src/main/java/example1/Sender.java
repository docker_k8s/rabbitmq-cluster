package example1;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by zaletel on 31.3.2017.
 */
public class Sender {

    private final static String QUEUE_NAME = "ha.hello";

    public static void main(String[] argv) throws Exception {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername("user");
        factory.setPassword("password");
        Address[] address = {new Address("localhost", 5672), new Address("localhost", 5673), new Address("localhost", 5674)};
        Connection connection = factory.newConnection(address);
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        for (Integer i = 0; i<10000; i++) {
            //Thread.sleep(100);
            String message = "Hello World: " + i.toString();
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
            System.out.println(" [x] Sent '" + message + "'");
        }
        channel.close();
        connection.close();

        for (Integer i = 10000; i<20000; i++) {
            runMultiConnection(i);
        }
    }

    public static void runMultiConnection(Integer i) throws IOException, TimeoutException, InterruptedException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername("user");
        factory.setPassword("password");
        Address[] address = {new Address("localhost", 5672), new Address("localhost", 5673), new Address("localhost", 5674)};
        Connection connection = factory.newConnection(address);
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
            Thread.sleep(100);
            String message = "Hello multi World: " + i.toString();
            channel.basicPublish("", QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes("UTF-8"));
            System.out.println(" [x] Sent '" + message + "'");
        channel.close();
        connection.close();

    }
}
